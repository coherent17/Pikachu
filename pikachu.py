import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score

class Pikachu():

    def __init__(self):
        pass

    def polyfit_trend_line_poly1(x,y):
        z=np.polyfit(x,y,1)
        p=np.poly1d(z)
        r2=r2_score(y,p(x))
        return p,r2,z

    def plot_trend_line_poly1(x,y,p,z,is_grid=False):
        fig=plt.figure()
        ax=fig.add_subplot(111)
        ax.plot(x,y,"o-",label='data line')
        ax.plot(x,p(x),"r--",label='trend line')
        if z[1]<0:
            ax.text(2.5,5,"y=%.4fx%.4f" %(z[0],z[1]))
        else:
            ax.text(2.5,5,"y=%.4fx+%.4f" %(z[0],z[1]))
        ax.text(2.5,4,"$R^2$=%.6f" %(r2))
        ax.grid(is_grid)
        ax.legend()
        return fig,ax

    def polyfit_trend_line_poly2(x,y):
        z=np.polyfit(x,y,2)
        p=np.poly1d(z)
        r2=r2_score(y,p(x))
        return p,r2,z

    def plot_trend_line_poly2(x,y,p,z,is_grid=False):
        fig=plt.figure()
        ax=fig.add_subplot(111)
        ax.plot(x,y,"o-",label='data line')
        ax.plot(x,p(x),"r--",label='trend line')
        ax.text(2.5,5,"y=%.4f$x^2$+%.4fx+%.4f" %(z[0],z[1],z[2]))
        ax.text(2.5,4,"$R^2$=%.6f" %(r2))
        ax.grid(is_grid)
        ax.legend()
        return fig,ax

    def plot_horizontal_line(num,ls="--",clr="g"):
        plt.axhline(y=num,linestyle=ls,color=clr)

    def plot_vertical_line(num,ls="--",clr="k"):
        plt.axvline(x=num,linestyle=ls,color=clr)

if __name__=='__main__':
    x=np.array([1,2,3,4,5])
    y=np.array([2,3,9,12,17])
    p,r2,z=Pikachu.polyfit_trend_line_poly1(x,y)
    fig,ax=Pikachu.plot_trend_line_poly1(x,y,p,z,True)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_title("this is title")
    Pikachu.plot_horizontal_line(3)
    Pikachu.plot_vertical_line(4)
    plt.show()
    p,r2,z=Pikachu.polyfit_trend_line_poly2(x,y)
    fig,ax=Pikachu.plot_trend_line_poly2(x,y,p,z,True)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_title("this is title")
    Pikachu.plot_horizontal_line(3)
    Pikachu.plot_vertical_line(4)
    plt.show()